#include "taskmaster.h"

int		set_cmd(t_process *proc, void *val)
{
	proc->cmd = strdup((char *)val);
	return (1);
}

int		set_workingdir(t_process *proc, void *val)
{
	proc->workingdir = strdup((char *)val);
	return (1);
}

int		set_autostart(t_process *proc, void *val)
{
	if (strcmp((char *)val, "true") == 0)
		proc->autostart = TRUE;
	else if (strcmp((char *)val, "false") == 0)
		proc->autostart = FALSE;
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_autorestart(t_process *proc, void *val)
{
	if (strcmp((char *)val, "unexpected") == 0)
		proc->autorestart = UNEXPECTED;
	else if (strcmp((char *)val, "never") == 0)
		proc->autorestart = NEVER;
	else if (strcmp((char *)val, "always") == 0)
		proc->autorestart = ALWAYS;
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_startretries(t_process *proc, void *val)
{
	if (atoi((char *)val) != 0)
		proc->startretries = atoi((char *)val);
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_numprocs(t_process *proc, void *val)
{
	if (atoi((char *)val) != 0)
		proc->numprocs = atoi((char *)val);
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_starttime(t_process *proc, void *val)
{
	if (isdigit(*(char *)val))
		proc->starttime = atoi((char *)val);
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_stopsignal(t_process *proc, void *val)
{
	//if (convert_signum((char *)val) > 0)
		//proc->stopsignal = convert_signum((char *)val);
	if (1)
		proc->stopsignal = atoi((char*)val);
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_stoptime(t_process *proc, void *val)
{
	if (atoi((char *)val) != 0)
		proc->stoptime = atoi((char *)val);
	else
	{
		parse_errno.err_code = INVALID_VAL;
		return (-1);
	}
	return (1);
}

int		set_exitcodes(t_process *proc, void *val)
{
	proc->exit_codes = val;
	return (1);
}
