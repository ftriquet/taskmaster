#include "taskmaster.h"

t_process	*new_process(char *prog_name)
{
	t_process	*new;

	if (!(new = (t_process *)malloc(sizeof(*new))))
	{
		errno = ENOMEM;
		return (NULL);
	}
	new->name = strdup(prog_name);
	new->cmd = NULL;
	new->workingdir = NULL;
	new->numprocs = 1;
	new->autostart = TRUE;
	new->autorestart = UNEXPECTED;
	new->exit_codes = NULL;
	new->starttime = 1;
	new->startretries = 1;
	new->stopsignal = SIGINT;
	new->stoptime = 1;
	new->stdout_log.filename = NULL;
	new->stdout_log.fd = STDOUT_FILENO;
	new->stderr_log.filename = NULL;
	new->stderr_log.fd = STDERR_FILENO;
	new->env = NULL;
	return (new);
}

int			set_process_attr(t_process *proc, char *key, void *val)
{
	if (strcmp(key, "cmd") == 0 &&
			set_cmd(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "workingdir") == 0 &&
			set_workingdir(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "autostart") == 0 &&
			set_autostart(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "autorestart") == 0 &&
			set_autorestart(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "startretries") == 0 &&
			set_startretries(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "numprocs") == 0 &&
			set_numprocs(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "starttime") == 0 &&
			set_starttime(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "stopsignal") == 0 &&
			set_stopsignal(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "stoptime") == 0 &&
			set_stoptime(proc, val) < 0)
		return (-1);
	else if (strcmp(key, "exitcodes") == 0)
		set_exitcodes(proc, val);
	else if (strcmp(key, "stdout") == 0)
		proc->stdout_log.filename = strdup((char *)val);
	else if (strcmp(key, "stderr") == 0)
		proc->stderr_log.filename = strdup((char *)val);
	return (1);
}

int			print_process(t_process *proc)
{
	printf("======================\n");
	printf("name : %s\n", proc->name);
	printf("cmd : %s\n", proc->cmd);
	printf("workingdir : %s\n", proc->workingdir);
	printf("numprocs : %u\n", proc->numprocs);
	printf("autostart : %d\n", proc->autostart);
	printf("autorestart : %d\n", proc->autorestart);
	printf("starttime : %lu\n", proc->starttime);
	printf("startretries : %u\n", proc->startretries);
	printf("stopsignal : %u\n", proc->stopsignal);
	printf("stoptime : %lu\n", proc->stoptime);
	printf("out : %s\n", proc->stdout_log.filename);
	printf("err : %s\n", proc->stderr_log.filename);
}
