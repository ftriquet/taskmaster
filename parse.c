#include "taskmaster.h"
#include <yaml.h>
#include <stdio.h>

void		assert_parsing(int val)
{
	if (val == 0)
	{
		fprintf(stderr, "Parsing error\n");
		exit(1);
	}
}

t_process	*parse(char *filename)
{
	FILE			*ip;
	yaml_parser_t	parser;
	yaml_token_t	token;
	t_process		*process_list;
	char			*process_name;

	process_list = NULL;
	ip = fopen(filename, "r");
	/* Initialize parser */
	if (!yaml_parser_initialize(&parser))
	{
		fputs("Failed to initialize parser\n", stderr);
		return (NULL);
	}
	if (!ip)
	{
		fputs("Failed to open config file\n", stderr);
		return (NULL);
	}

	/* Set input file */
	yaml_parser_set_input_file(&parser, ip);

	// Traitement
	do
	{
		yaml_parser_scan(&parser, &token);
		switch (token.type)
		{
			// Stream start/end
			case YAML_STREAM_START_TOKEN:
			case YAML_STREAM_END_TOKEN:
			case YAML_BLOCK_MAPPING_START_TOKEN:
			case YAML_BLOCK_END_TOKEN:
				break;
				// Token type
			case YAML_KEY_TOKEN:
				yaml_parser_scan(&parser, &token);
				if (token.type == YAML_SCALAR_TOKEN)
				{
					process_name = strdup((char *)token.data.scalar.value);
					yaml_parser_scan(&parser, &token);
					yaml_parser_scan(&parser, &token);
					if (token.type == YAML_BLOCK_MAPPING_START_TOKEN)
					{
						parse_process(&parser, &process_list, process_name);
					}
				}
				break;
			default:
				printf("Token of type %d\n", token.type);
		}
		if (token.type != YAML_STREAM_END_TOKEN)
			yaml_token_delete(&token);
	} while (token.type != YAML_STREAM_END_TOKEN);

	/* Cleaning */
	yaml_parser_delete(&parser);
	fclose(ip);
	t_process *tmp = process_list;
	int			print_process(t_process *proc);
	while (tmp)
	{
		print_process(tmp);
		tmp = tmp->next;
	}
	return (process_list);
}

int		parse_process(yaml_parser_t *parser, t_process **proc, char *name)
{
	t_process		*new_proc = new_process(name);
	char			*key;
	void			*val;
	yaml_token_t	token;

	yaml_parser_scan(parser, &token);
	assert_parsing(token.type == YAML_KEY_TOKEN);
	do
	{
		yaml_parser_scan(parser, &token);
		assert_parsing(token.type == YAML_SCALAR_TOKEN);
		key = strdup(token.data.scalar.value);
		if (strcmp(key, "exitcodes") == 0)
			val = get_exit_codes(parser);
		else
		{
			yaml_parser_scan(parser, &token);
			assert_parsing(token.type == YAML_VALUE_TOKEN);
			yaml_parser_scan(parser, &token);
			assert_parsing(token.type == YAML_SCALAR_TOKEN);
			val = strdup(token.data.scalar.value);
		}
		if (set_process_attr(new_proc, key, val) < 0)
		{
			fprintf(stderr, "Parsing error\n");
			exit(1);
		}
		free(key);
		free(val);
		yaml_parser_scan(parser, &token);
	} while (token.type != YAML_BLOCK_END_TOKEN);
	new_proc->next = *proc;
	*proc = new_proc;
	return (1);
}

int		*get_exit_codes(yaml_parser_t *parser)
{
	yaml_token_t	token;
	int				*codes = NULL;
	int				*tmp;
	int				size = 1;

	yaml_parser_scan(parser, &token);
	assert_parsing(token.type == YAML_VALUE_TOKEN);
	yaml_parser_scan(parser, &token);
	if (token.type == YAML_SCALAR_TOKEN)
	{
		codes = (int *)malloc(sizeof(*codes) * (size + 1));
		codes[1] = -1;
	}
	else
	{
		size = 0;
		assert_parsing(token.type == YAML_BLOCK_SEQUENCE_START_TOKEN);
		yaml_parser_scan(parser, &token);
		assert_parsing(token.type == YAML_BLOCK_ENTRY_TOKEN);
		while (token.type != YAML_BLOCK_END_TOKEN)
		{
			yaml_parser_scan(parser, &token);
			assert_parsing(token.type == YAML_SCALAR_TOKEN);
			tmp = (int *)malloc(sizeof(*codes) * (size + 1));
			if (codes)
				for (int i = 0; i < size; ++i)
					tmp[i] = codes[i];
			tmp[size] = atoi(token.data.scalar.value);
			tmp[size + 1] = -1;
			if (codes)
				free(codes);
			codes = tmp;
			yaml_parser_scan(parser, &token);
			assert_parsing(token.type == YAML_BLOCK_ENTRY_TOKEN ||
					token.type == YAML_BLOCK_END_TOKEN);
		}
	}
	return (codes);
}
