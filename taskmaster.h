#ifndef TASKMASTER_H
# define TASKMASTER_H
# include <sys/types.h>
# include <yaml.h>
# include <string.h>
# include <errno.h>
# include <unistd.h>
# include <stdlib.h>
# include <ctype.h>


#define INVALID_VAL 0
#define INVALID_KEY 1
#define PARSING_ERROR 2
#define DEBUG printf("%s in %s at line %d\n", __func__, __FILE__, __LINE__);
typedef struct			s_parse_err
{
	int					err_code;
	char				*err_str;
}						t_parse_err;

t_parse_err				parse_errno;

typedef enum			e_bool
{
	FALSE = 0,
	TRUE
}						t_bool;

typedef enum			e_exit_mode
{
	ALWAYS,
	NEVER,
	UNEXPECTED
}						t_exit_mode;

typedef struct			s_log
{
	int					fd;
	char				*filename;
}						t_log;

typedef struct			s_env
{
	char				*key;
	char				*val;
	struct s_env		*next;
}						t_env;

typedef struct			s_process
{
	char				*name;
	char				*cmd;
	char				*workingdir;
	unsigned int		numprocs;
	t_bool				autostart;
	t_exit_mode			autorestart;
	int					*exit_codes;
	unsigned long		starttime;
	unsigned int		startretries;
	unsigned int		stopsignal;
	unsigned long		stoptime;
	t_log				stdout_log;
	t_log				stderr_log;
	t_env				*env;
	char				*umask;
	struct s_process	*next;
}						t_process;

//parse.c
t_process		*parse(char *filename);
int				parse_process(yaml_parser_t *parser, t_process **proc, char *name);
int				*get_exit_codes(yaml_parser_t *parser);
t_process		*parse(char *filename);


// SETTERS
int		set_cmd(t_process *proc, void *val);
int		set_workingdir(t_process *proc, void *val);
int		set_autostart(t_process *proc, void *val);
int		set_autorestart(t_process *proc, void *val);
int		set_startretries(t_process *proc, void *val);
int		set_numprocs(t_process *proc, void *val);
int		set_starttime(t_process *proc, void *val);
int		set_stopsignal(t_process *proc, void *val);
int		set_stoptime(t_process *proc, void *val);
int		set_exitcodes(t_process *proc, void *val);

// Process.c
int			set_process_attr(t_process *proc, char *key, void *val);
t_process	*new_process(char *prog_name);
#endif
